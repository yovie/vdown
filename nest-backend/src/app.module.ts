import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SearcherController } from './module/searcher/searcher.controller';
import { DownloaderController } from './module/downloader/downloader.controller';

@Module({
  imports: [],
  controllers: [AppController, SearcherController, DownloaderController],
  providers: [AppService],
})
export class AppModule {}
