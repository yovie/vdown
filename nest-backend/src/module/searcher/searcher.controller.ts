import { Controller, Get, Req, Res, HttpStatus, Query } from '@nestjs/common';
import youtube from 'scrape-youtube';
import { Response, Request } from 'express';

@Controller('searcher')
export class SearcherController {

	@Get('youtube')
	searchYoutube(@Req() req: Request, @Res() res: Response, @Query('q') q: string) {
		youtube.search(q).then(result => {
			res.status(HttpStatus.FOUND).json(result);
		});
	}
}
