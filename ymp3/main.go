package main

import (
	"reflect"
	"ymp3/controller"
	"ymp3/library"
)

func main() {

	// load all configuration
	configGlobal := library.ParseFile("config/config.json", reflect.TypeOf(library.ConfigGlobal{})).(library.ConfigGlobal)
	configDevelopment := library.ParseFile("config/development.json", reflect.TypeOf(library.ConfigHost{})).(library.ConfigHost)
	configProduction := library.ParseFile("config/production.json", reflect.TypeOf(library.ConfigHost{})).(library.ConfigHost)
	configRoutes := library.ParseFile("config/routes.json", reflect.TypeOf(library.ConfigRoutes{})).(library.ConfigRoutes)

	library.TheKey = []byte(configGlobal.Key)

	r := library.SetupRouter(controller.ControllerRegistry, configRoutes, configGlobal.Base)

	if configGlobal.Mode == library.ModeDevelopment {
		r.Run(":" + configDevelopment.Port)
	} else {
		r.Run(":" + configProduction.Port)
	}
}
