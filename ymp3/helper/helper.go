package helper

import (
	"fmt"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

// CreateToken create the token
func CreateToken(theKey []byte) (string, error) {
	now := time.Now()
	tomorrow := now.AddDate(0, 0, 1)

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user_id": "1",
		// "nbf":     time.Date(2019, 10, 10, 12, 0, 0, 0, time.UTC).Unix(),
		"exp": tomorrow.Unix(),
	})

	tokenString, err := token.SignedString(theKey)

	return tokenString, err
}

// CreateTokenTM create the token
func CreateTokenTM(theKey []byte) (string, error) {
	now := time.Now()
	tomorrow := now.AddDate(0, 0, 1)

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"time": now.Unix(),
		"exp":  tomorrow.Unix(),
	})

	tokenString, err := token.SignedString(theKey)

	return tokenString, err
}

// ValidateTokenTM create the token
func ValidateTokenTM(theKey []byte, tokenS string) (bool, error) {
	tokenP, err := jwt.Parse(tokenS, func(token *jwt.Token) (interface{}, error) {
		if errk, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			if errk != nil {
				fmt.Println("ada error")
			}
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return theKey, nil
	})

	if _, ok := tokenP.Claims.(jwt.MapClaims); ok && tokenP.Valid {
		// fmt.Println(claims)
		return true, nil
	} else {
		return false, err
	}
}
