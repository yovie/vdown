package library

import (
	"fmt"
	"net/http"
	"strings"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

// TheKey sample
var TheKey = []byte("")

// MiddlewareAuthJWT middleware for jwt auth
func MiddlewareAuthJWT(c *gin.Context, m string, f gin.HandlerFunc) {

	authHeader := c.GetHeader("Authorization")

	if authHeader == "" {
		c.JSON(http.StatusUnauthorized, gin.H{"status": http.StatusUnauthorized, "message": "Authorization Token is required"})
		c.Abort()
		return
	}

	tokenS := strings.Split(authHeader, "Bearer ")

	if len(tokenS) != 2 {
		c.JSON(http.StatusUnauthorized, gin.H{"status": http.StatusUnauthorized, "message": "Invalid token"})
		c.Abort()
		return
	}

	tokenP, err := jwt.Parse(tokenS[1], func(token *jwt.Token) (interface{}, error) {
		if errk, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			if errk != nil {
				fmt.Println("ada error")
			}
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return TheKey, nil
	})

	if claims, ok := tokenP.Claims.(jwt.MapClaims); ok && tokenP.Valid {
		fmt.Println(claims["user_id"], claims["nbf"])
		f(c)

	} else {
		c.JSON(http.StatusUnauthorized, gin.H{"status": http.StatusUnauthorized, "message": err.Error()})
		c.Abort()
		return
	}

}
