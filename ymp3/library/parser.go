package library

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
)

// ParseFile parse json config files
func ParseFile(f string, s reflect.Type) interface{} {
	appConfig, err := os.Open(f)

	if err != nil {
		fmt.Println(err)
	}

	defer appConfig.Close()

	byteValue, _ := ioutil.ReadAll(appConfig)

	if s.Name() == StructConfigGlobal {
		var result ConfigGlobal
		json.Unmarshal([]byte(byteValue), &result)
		return result
	}

	if s.Name() == StructConfigHost {
		var result ConfigHost
		json.Unmarshal([]byte(byteValue), &result)
		return result
	}

	if s.Name() == StructConfigRoutes {
		var result ConfigRoutes
		json.Unmarshal([]byte(byteValue), &result)
		return result
	}

	return nil
}
