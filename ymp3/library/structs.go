package library

// StructConfigGlobal global config structure
const StructConfigGlobal = "ConfigGlobal"

// StructConfigHost host descriptor structure
const StructConfigHost = "ConfigHost"

// StructConfigRoutes all route config
const StructConfigRoutes = "ConfigRoutes"

// ModeDevelopment development mode
const ModeDevelopment = "DEVELOPMENT"

// ModeProduction production mode
const ModeProduction = "PRODUCTION"

// MethodGet GET
const MethodGet = "GET"

// MethodPost POST
const MethodPost = "POST"

// MethodPut PUT
const MethodPut = "PUT"

// MethodDelete DELETE
const MethodDelete = "DELETE"

// MethodHead HEAD
const MethodHead = "HEAD"

// AuthJWT auth by JWT
const AuthJWT = "JWT"

// AuthOAuth2 auth OAuth
const AuthOAuth2 = "OAUTH2"

// ConfigGlobal struct
type ConfigGlobal struct {
	Title string `json:"title"`
	Mode  string `json:"mode"`
	Key   string `json:"key"`
	Base  string `json:"base"`
}

// ConfigHost struct
type ConfigHost struct {
	BaseURL    string `json:"base_url"`
	Port       string `json:"port"`
	DbType     string `json:"db_type"`
	DbHost     string `json:"db_host"`
	DbUser     string `json:"db_user"`
	DbPassword string `json:"db_password"`
	DbName     string `json:"db_name"`
}

// ConfigRoute struct
type ConfigRoute struct {
	Path     string `json:"path"`
	Auth     string `json:"auth"`
	Method   string `json:"method"`
	Function string `json:"function"`
}

// ConfigRoutes struct
type ConfigRoutes struct {
	Routes []ConfigRoute `json:"routes"`
}
