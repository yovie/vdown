package library

import (
	"github.com/gin-gonic/gin"
)

// SetupRouter setup all route
func SetupRouter(registry map[string]gin.HandlerFunc, s ConfigRoutes, b string) *gin.Engine {
	r := gin.New()

	r.Use(gin.Logger())

	r.Use(gin.Recovery())

	for _, route := range s.Routes {

		if route.Auth != "" {
			cc := registry[route.Function]
			ff := func(c *gin.Context) {
				MiddlewareAuthJWT(c, route.Auth, cc)
			}
			r.Handle(route.Method, b+route.Path, ff)
		} else {
			r.Handle(route.Method, b+route.Path, registry[route.Function])
		}

	}

	return r
}
