module ymp3

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/rylio/ytdl v0.6.3
	github.com/wader/goutubedl v0.0.0-20200616180254-92c7a86a3ab3
	google.golang.org/api v0.29.0
)
