package welcome

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"ymp3/helper"
	"ymp3/library"
)

// Index return json
func Index(c *gin.Context) {
	token, _ := helper.CreateTokenTM(library.TheKey)

	c.JSON(http.StatusOK, gin.H{"result": "success", "token": token})
}
