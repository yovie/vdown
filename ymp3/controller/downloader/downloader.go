package downloader

import (
	"bufio"
	"bytes"
	"context"
	"flag"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"ymp3/helper"
	"ymp3/library"

	"github.com/gin-gonic/gin"

	"github.com/rylio/ytdl"
	"github.com/wader/goutubedl"

	"google.golang.org/api/googleapi/transport"

	youtube "google.golang.org/api/youtube/v3"
)

var maxResults = flag.Int64("max-results", 10, "Max YouTube results")

// DownloadV1 download process v1
func DownloadV1(c *gin.Context) {

	ctx := context.Background()
	client := ytdl.DefaultClient

	videoInfo, err := client.GetVideoInfo(ctx, "https://www.youtube.com/watch?v=u8beEIZfChs")
	if err != nil {
		panic(err)
	}

	file, err := os.Create(videoInfo.Title + ".mp4")
	if err != nil {
		panic(err)
	}

	err = client.Download(ctx, videoInfo, videoInfo.Formats[0], file)
	if err != nil {
		panic(err)
	}

	targetPath := filepath.Join("./", videoInfo.Title+".mp4")

	c.Header("Content-Description", "File Transfer")
	c.Header("Content-Transfer-Encoding", "binary")
	c.Header("Content-Disposition", "attachment; filename="+videoInfo.Title+".mp4")
	c.Header("Content-Type", "application/octet-stream")
	c.File(targetPath)
}

// DownloadV2 download process v1, success relay download youtube video
func DownloadV2(c *gin.Context) {

	ctx := context.Background()
	client := ytdl.DefaultClient

	videoInfo, err := client.GetVideoInfo(ctx, "https://www.youtube.com/watch?v=u8beEIZfChs")
	if err != nil {
		panic(err)
	}

	var buffer bytes.Buffer
	writer := bufio.NewWriter(&buffer)

	err = client.Download(ctx, videoInfo, videoInfo.Formats[0], writer)
	if err != nil {
		panic(err)
	}

	c.Header("Content-Description", "File Transfer")
	c.Header("Content-Transfer-Encoding", "binary")
	c.Header("Content-Disposition", "attachment; filename="+videoInfo.Title+".mp4")
	c.Data(http.StatusOK, "application/octet-stream", buffer.Bytes())
}

// DownloadV3 relay download facebook video, support facebook video & youtube video
func DownloadV3(c *gin.Context) {

	// videoURL := "https://www.facebook.com/video.php?v=10152588878600983"

	// videoURL := "https://www.youtube.com/watch?v=u8beEIZfChs"

	videoURL := "https://web.facebook.com/1120342544711874/videos/268795354376265/"

	// Get video data
	res, err := goutubedl.New(context.Background(), videoURL, goutubedl.Options{})
	if err != nil {
		panic(err)
	}

	file := res.Info.Title + ".mp4"

	videoStream, err := res.Download(context.Background(), "best")
	if err != nil {
		panic(err)
	}
	defer videoStream.Close()

	var buffer bytes.Buffer
	writer := bufio.NewWriter(&buffer)

	io.Copy(writer, videoStream)

	c.Header("Content-Description", "File Transfer")
	c.Header("Content-Transfer-Encoding", "binary")
	c.Header("Content-Disposition", "attachment; filename="+file)
	c.Data(http.StatusOK, "application/octet-stream", buffer.Bytes())
}

// Download relay download facebook video, support facebook video & youtube video
func Download(c *gin.Context) {

	token := c.DefaultQuery("key", "empty")

	uri := c.DefaultQuery("url", "")

	videoURL, err := url.PathUnescape(uri)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"result": "failed", "message": err.Error()})
		return
	}

	valid, err := helper.ValidateTokenTM(library.TheKey, token)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"result": "failed", "message": err.Error()})
		return
	}

	if valid == false {
		c.JSON(http.StatusOK, gin.H{"result": "failed", "message": "Invalid key"})
		return
	}

	// Get video data
	res, err := goutubedl.New(context.Background(), videoURL, goutubedl.Options{})
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"result": "failed", "message": err.Error()})
		return
	}

	file := res.Info.Title + ".mp4"

	videoStream, err := res.Download(context.Background(), "best")
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"result": "failed", "message": err.Error()})
		return
	}
	defer videoStream.Close()

	var buffer bytes.Buffer
	writer := bufio.NewWriter(&buffer)

	io.Copy(writer, videoStream)

	c.Header("Content-Description", "File Transfer")
	c.Header("Content-Transfer-Encoding", "binary")
	c.Header("Content-Disposition", "attachment; filename="+file)
	c.Data(http.StatusOK, "application/octet-stream", buffer.Bytes())
}

// VideoInfo get video info
func VideoInfo(c *gin.Context) {
	token := c.DefaultQuery("key", "empty")

	uri := c.DefaultQuery("url", "")

	videoURL, err := url.PathUnescape(uri)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"result": "failed", "message": err.Error()})
		return
	}

	valid, err := helper.ValidateTokenTM(library.TheKey, token)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"result": "failed", "message": err.Error()})
		return
	}

	if valid == false {
		c.JSON(http.StatusOK, gin.H{"result": "failed", "message": "Invalid key"})
		return
	}

	res, err := goutubedl.New(context.Background(), videoURL, goutubedl.Options{})
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"result": "failed", "message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"result": "success", "data": res.Info})
}

// SearchVideo search video
func SearchVideo(c *gin.Context) {
	const developerKey = "AIzaSyB-B_bTQxhwbIGCz4XWYVmOBKCZl4BUoiI"

	token := c.DefaultQuery("key", "empty")

	q := c.DefaultQuery("q", "")

	p := c.DefaultQuery("page", "")

	qq, err := url.PathUnescape(q)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"result": "failed", "message": err.Error()})
		return
	}

	valid, err := helper.ValidateTokenTM(library.TheKey, token)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"result": "failed", "message": err.Error()})
		return
	}

	if valid == false {
		c.JSON(http.StatusOK, gin.H{"result": "failed", "message": "Invalid key"})
		return
	}

	client := &http.Client{
		Transport: &transport.APIKey{Key: developerKey},
	}

	service, err := youtube.New(client)
	if err != nil {
		log.Fatalf("Error creating new YouTube client: %v", err)
	}

	response := new(youtube.SearchListResponse)
	part := []string{"id,snippet"}

	call := service.Search.List(part).
		Q(qq).
		MaxResults(*maxResults)

	if p == "" {
		response, err = call.Do()
		if err != nil {
			log.Fatalf("Error searching: %v", err)
		}
	}

	if p != "" {
		call = call.PageToken(p)
		response, err = call.Do()
		if err != nil {
			log.Fatalf("Error on page: %v", err)
		}
	}

	videos := make(map[string]string)
	channels := make(map[string]string)
	playlists := make(map[string]string)

	// jm, _ := response.MarshalJSON()
	// fmt.Println(string(jm))

	for _, item := range response.Items {
		mjson, _ := item.Snippet.MarshalJSON()
		// pjson, _ := json.Marshal(mjson)
		// fmt.Println(string(mjson))

		switch item.Id.Kind {
		case "youtube#video":
			videos[item.Id.VideoId] = string(mjson)
		case "youtube#channel":
			channels[item.Id.ChannelId] = string(mjson)
		case "youtube#playlist":
			playlists[item.Id.PlaylistId] = string(mjson)
		}
	}

	c.JSON(
		http.StatusOK, gin.H{
			"result":    "success",
			"videos":    videos,
			"channels":  channels,
			"playlists": playlists,
			"prevPage":  response.PrevPageToken,
			"nextPage":  response.NextPageToken})
}
