package controller

import (
	"github.com/gin-gonic/gin"

	"ymp3/controller/downloader"
	"ymp3/controller/welcome"
)

// ControllerRegistry ref for controller struct
var ControllerRegistry = make(map[string]gin.HandlerFunc)

func init() {
	ControllerRegistry["welcome/Index"] = welcome.Index
	ControllerRegistry["downloader/Download"] = downloader.Download
	ControllerRegistry["downloader/VideoInfo"] = downloader.VideoInfo
	ControllerRegistry["downloader/SearchVideo"] = downloader.SearchVideo
}
