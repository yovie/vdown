import React, { Component } from 'react';
import { Card, Button, FormControl } from 'react-bootstrap';
// import * as ytdl from 'ytdl-core';

export default class Downloader extends Component {

	token = null;

	searchResult;

	constructor(props) {
		super(props);
		this.state = { keyword: '' };
		this.getToken();
	}

	onChangeKeyword = (event) => {
		this.setState({ keyword: event.target.value })
	}

	getToken = () => {
		fetch('/api').then(res => res.json()).then(result => {
			this.token = result.token;
		});
	}

	keyPress = (e) => {
		if (e.keyCode == 13) {
			this.submit();
		}
	}

	submit = (pg) => {
		let qPage = '';
		if (pg !== undefined) {
			qPage = '&page=' + pg;
		}

		this.setState({ loading: true });

		fetch('/api/search?key=' + this.token + '&q=' + this.state.keyword + qPage).then(res => res.json()).then(result => {
			this.setState({ keyword: this.state.keyword, result: result, loading: false });
		});
	}

	download = (id) => {
		// ytdl('https://www.youtube.com/watch?v=' + id, { format: 'mp4' });
		window.open('/api/download?key=' + this.token + '&url=https://www.youtube.com/watch?v=' + id, '_blank');
	}

	render() {
		if (this.state.result && this.state.result.videos) {
			const vkey = Object.keys(this.state.result.videos);
			this.searchResult = vkey.map((vid, idx) => {
				const data = JSON.parse(this.state.result.videos[vid]);
				let uri;
				if (uri === undefined) {
					uri = data.thumbnails.high.url;
				}
				if (uri === undefined) {
					uri = data.thumbnails.medium.url;
				}
				if (uri === undefined) {
					uri = data.thumbnails.default.url;
				}
				console.log(data);
				return (
					<div className="col-md-6 mt-4">
						<Card>
							<Card.Body>
								<Button size="md" variant="success" onClick={() => this.download(vid)}>Download</Button>
								<img src={uri} alt={data.title} width="100%" />
								<p className="float-title">{data.title}</p>
							</Card.Body>
						</Card>
					</div>
				)
			});
		}
		return (
			<div className="container">
				<div className="col-md-12 mt-4 row">
					<div className="col-md-12">
						<Card>
							<Card.Body>
								<Card.Title>Video Downloader</Card.Title>
								<Card.Text>
									<FormControl
										placeholder="Search keyword"
										aria-label="Search keyword"
										onChange={this.onChangeKeyword}
										onKeyDown={this.keyPress}
									/>
								</Card.Text>
								<div className="row">
									<div className="col-md-4">
										{this.state.loading === true ? <img className="float-loading" src="loading.gif" height="70px" /> : ""}
										{this.state.result && this.state.result.prevPage !== "" ?
											<Button size="md" variant="warning" onClick={() => this.submit(this.state.result.prevPage)}>Prev Video</Button> : ""}
									</div>
									<div className="col-md-4">
										<Button size="lg" variant="primary" onClick={() => this.submit()}>Submit</Button>
									</div>
									<div className="col-md-4">
										{this.state.result ? <Button size="md" variant="warning" onClick={() => this.submit(this.state.result.nextPage)}>Next Video</Button> : ""}
									</div>
								</div>
							</Card.Body>
						</Card>
					</div>
				</div>
				<div className="col-md-12 mt-4 row">
					{this.searchResult}
				</div>
			</div>
		)
	}
}