import React from 'react';
import Downloader from './Downloader.js'
import './App.css';

function App() {
	return (
		<div className="App">
			<Downloader />
		</div>
	);
}

export default App;
